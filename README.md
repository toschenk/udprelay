# UDP Relay

Tiny command-line tool which relays incoming UDP packets on a network interface to a different
network interface.

## Problem description

Many games use UDP broadcasts (only) to discover servers to which they can connect to.
In some cases when there are multiple network interfaces, the game sends its discovery message
to the wrong broadcast address. This tool intercepts these messages and relays them to the desired interface
so that the server can actually be discovered.

## Solution

The tool accepts three command line arguments:

```
$ udprelay <target-ifname> <source-ifname> <port>
```

``target-ifname`` is the interface name which is the desired interface to send discovery messages to (f.ex. tap0)
``source-ifname`` is the interface name to which the game sends its discovery messages to (f.ex. eth0)
``port`` is the discovery message target port (f.ex. 1234)

Whenever a packet is received on the source interface, it is re-sent on the target interface.
The server, which must be reachable at the target interface, will then reply back to the target interface.
We just have to hope that the game is also listening on the target interface and not just on the source interface
(where it sends its discovery packets to), otherwise additional hackery would be required.

