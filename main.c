#include <net/if.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/ioctl.h>
#include <arpa/inet.h>
#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>
#include <errno.h>
#include <string.h>
#include <unistd.h>
#include <poll.h>

typedef struct iface iface_t;

struct iface {
	const char *name;
	int index;
	int flags;
	int recv_fd;
	int send_fd;
	struct in_addr addr;
	struct in_addr broadcast;

	char addr_str[256];
	char broadcast_str[256];
};

static bool get_iface(
		int fd,
		iface_t *iface)
{
	struct ifreq req_base = { 0 };
	struct ifreq req;
	const char *str;
	strncpy(req_base.ifr_name, iface->name, IFNAMSIZ);

	/* get interface index */
	memcpy(&req, &req_base, sizeof(req));
	if(ioctl(fd, SIOCGIFINDEX, &req) < 0) {
		fprintf(stderr, "error getting index for interface '%s': %s\n", iface->name, strerror(errno));
		return false;
	}
	iface->index = req.ifr_ifindex;


	/* get interface flags */
	memcpy(&req, &req_base, sizeof(req));
	if(ioctl(fd,SIOCGIFFLAGS, &req) < 0) {
		fprintf(stderr, "error getting flags for interface '%s': %s\n", iface->name, strerror(errno));
		return false;
	}
	iface->flags = req.ifr_flags;

	/* get local address */
	memcpy(&req, &req_base, sizeof(req));
	if(ioctl(fd, SIOCGIFADDR, &req) < 0) {
		fprintf(stderr, "error getting local address for interface '%s': %s\n", iface->name, strerror(errno));
		return false;
	}
	memcpy(&iface->addr, &((struct sockaddr_in *)&req.ifr_addr)->sin_addr, sizeof(iface->addr));
	str = inet_ntoa(iface->addr);
	strncpy(iface->addr_str, str, sizeof(iface->addr_str) - 1);

	/* get broadcast/destination address */
	memcpy(&req, &req_base, sizeof(req));
	if(iface->flags & IFF_BROADCAST) {
		if(ioctl(fd, SIOCGIFBRDADDR, &req) < 0) {
			fprintf(stderr, "error getting broadcast address for interface '%s': %s\n", iface->name, strerror(errno));
			return false;
		}
		memcpy(&iface->broadcast, &((struct sockaddr_in *)&req.ifr_broadaddr)->sin_addr, sizeof(iface->broadcast));
	}
	else {
		if(ioctl(fd, SIOCGIFDSTADDR, &req) < 0) {
			fprintf(stderr, "error getting destination address for interface '%s': %s\n", iface->name, strerror(errno));
			return false;
		}
		memcpy(&iface->broadcast, &((struct sockaddr_in *)&req.ifr_broadaddr)->sin_addr, sizeof(iface->broadcast));
	}
	str = inet_ntoa(iface->broadcast);
	strncpy(iface->broadcast_str, str, sizeof(iface->broadcast_str) - 1);

	return true;
}

static int yes = 1;
static int no = 0;

static bool open_target(
		iface_t *iface)
{
	int fd = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
	if(fd < 0) {
		fprintf(stderr, "error opening target interface socket: %s\n", strerror(errno));
		return false;
	}
	else if(
			setsockopt(fd, SOL_SOCKET, SO_BROADCAST, &yes, sizeof(yes)) < 0 ||
			setsockopt(fd, SOL_SOCKET, SO_REUSEPORT, &yes, sizeof(yes)) < 0 ||
			setsockopt(fd, SOL_SOCKET, SO_BINDTODEVICE, iface->name, strlen(iface->name) + 1) < 0)
	{
		fprintf(stderr, "error configuring target interface socket: %s\n", strerror(errno));
		return false;
	}

	iface->send_fd = fd;
	return true;
}

static bool open_source(
		iface_t *iface,
		int port)
{
	struct sockaddr_in bind_addr = { 0 };
	int fd = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
	if(fd < 0) {
		fprintf(stderr, "error opening source interface socket: %s\n", strerror(errno));
		return false;
	}
	else if(
			setsockopt(fd, SOL_SOCKET, SO_REUSEPORT, &yes, sizeof(yes)) < 0 ||
			setsockopt(fd, SOL_SOCKET, SO_BINDTODEVICE, iface->name, strlen(iface->name) + 1) < 0)
	{
		fprintf(stderr, "error configuring source interface socket: %s\n", strerror(errno));
		return false;
	}

	bind_addr.sin_family = AF_INET;
	bind_addr.sin_port = htons(port);
	//bind_addr.sin_addr = iface->addr;
	bind_addr.sin_addr.s_addr = INADDR_ANY;
	if(bind(fd, (struct sockaddr*)&bind_addr, sizeof(bind_addr)) < 0) {
		fprintf(stderr, "error binding source interface socket: %s\n", strerror(errno));
		return false;
	}

	iface->recv_fd = fd;
	return true;
}

static void mainloop(
		iface_t *target,
		iface_t *source,
		int port)
{
	unsigned char data[65536];
	struct in_addr from_addr;
	struct in_addr to_addr = target->broadcast;
	uint16_t to_port = htons(port);
	uint16_t from_port = 0;
	int npfd = 1;
	struct sockaddr_in addr = { 0 };
	struct pollfd pfd[2] = { 0 };

	pfd[0].fd = source->recv_fd;
	pfd[0].events = POLLIN;
	pfd[1].events = POLLIN;

	fprintf(stderr, "\nrelaying packets\n  incoming: %s\n", inet_ntoa(source->addr));
	fprintf(stderr, "  outgoing: from=%s", inet_ntoa(from_addr));
	fprintf(stderr, " to=%s\n\n", inet_ntoa(to_addr));
	for(;;) {
		uint16_t udp_len;
		uint16_t ip_len;
		socklen_t addr_len = sizeof(addr);
		int ret;

		ret = poll(pfd, npfd, 1000);
		if(ret <= 0)
			continue;
		if(pfd[0].revents) {
			int n = recvfrom(pfd[0].fd, data, sizeof(data), 0, (struct sockaddr*)&addr, &addr_len);
			if(n <= 0)
				continue;
			else if(from_port == addr.sin_port) {
				printf("received: %zd bytes from %s:%d\n", n, inet_ntoa(addr.sin_addr), ntohs(from_port));
				goto skip_open;
			}
			else if(from_port) {
				printf("received: %zd bytes from %s:%d, reopening back channel\n", n, inet_ntoa(addr.sin_addr), ntohs(addr.sin_port));
				close(source->send_fd);
				npfd--;
				from_port = addr.sin_port;
			}
			else {
				printf("received: %zd bytes from %s:%d, opening back channel\n", n, inet_ntoa(addr.sin_addr), ntohs(addr.sin_port));
				from_port = addr.sin_port;
			}

			from_addr = addr.sin_addr;
			if(!open_source(target, htons(from_port))) {
				fprintf(stderr, "  error opening source send socket\n");
				from_port = 0;
				continue;
			}
			else {
				pfd[1].fd = target->recv_fd;
				npfd++;
			}
skip_open:
			addr.sin_family = AF_INET;
			addr.sin_port = to_port;
			addr.sin_addr = to_addr;
			if(sendto(target->send_fd, data, n, 0, (struct sockaddr*)&addr, sizeof(addr)) < 0)
				fprintf(stderr, "  error sending packet: %s\n", strerror(errno));
		}
		if(npfd == 2 && pfd[1].revents) {
			int n = recvfrom(pfd[1].fd, data, sizeof(data), 0, (struct sockaddr*)&addr, &addr_len);
			if(n <= 0)
				continue;
			printf("received: %zd bytes from %s:%d\n", n, inet_ntoa(addr.sin_addr), ntohs(addr.sin_port));
			addr.sin_family = AF_INET;
			addr.sin_port = from_port;
			addr.sin_addr = from_addr;
			if(sendto(source->send_fd, data, n, 0, (struct sockaddr*)&addr, sizeof(addr)) < 0)
				fprintf(stderr, "  error sending packet: %s\n", strerror(errno));
		}
	}
}

int main(
		int argn,
		char **argv)
{
	iface_t source = { 0 };
	iface_t target = { 0 };
	int port;
	int fd;
	struct ifaddrs *if_list;
	if(argn != 4) {
		fprintf(stderr, "Usage: %s <target-if> <source-if> <port>\n", argv[0]);
		return 1;
	}
	source.name = argv[2];
	target.name = argv[1];
	port = atoi(argv[3]);

	fd = socket(AF_INET, SOCK_DGRAM, 0);
	if(fd < 0) {
		fprintf(stderr, "error opening raw socket, are you root?\n");
		return 1;
	}
	else if(!get_iface(fd, &source)) {
		fprintf(stderr, "error getting source interface\n");
		return 1;
	}
	else if(!get_iface(fd, &target)) {
		fprintf(stderr, "error getting target interface\n");
		return 1;
	}
	else if(!open_source(&source, port)) {
		fprintf(stderr, "error opening source interface receive socket\n");
		return 1;
	}
	else if(!open_target(&target)) {
		fprintf(stderr, "error opening target interface send socket\n");
		return 1;
	}
	else if(!open_target(&source)) {
		fprintf(stderr, "error opening source interface send socket\n");
		return 1;
	}
	close(fd);

	fprintf(stderr, "interfaces opened:\n");
	fprintf(stderr, "  target: name=%s index=%d addr=%s broadcast=%s\n", target.name, target.index, target.addr_str, target.broadcast_str);
	fprintf(stderr, "  source: name=%s index=%d addr=%s broadcast=%s\n", source.name, source.index, source.addr_str, source.broadcast_str);

	mainloop(&target, &source, port);
}

//test:
//  udprelay eth0 lo 12345
//  socat - udp-sendto:127.0.0.1:12345,broadcast

